import {createAppContainer} from 'react-navigation'
import {createStackNavigator} from 'react-navigation-stack'
import Dashboard from "./dashboard/containers/Dashboard"
import {store} from './dashboard/store/store'
import {Provider} from 'react-redux'
import React from "react";
import Devices from "./dashboard/containers/Devices"

const MainNavigator = createStackNavigator({
        Dashboard: Dashboard,
        Devices: Devices,
    },
    {
        initialRouteName: 'Dashboard',
    }
)

const Navigation = createAppContainer(MainNavigator)

export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <Navigation/>
            </Provider>
        );
    }
}


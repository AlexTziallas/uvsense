import React from 'react';
import {Provider} from 'react-redux';

import {store} from './dashboard/store/store';
import Dashboard from "./dashboard/containers/Dashboard";
import 'react-native-gesture-handler';

export default function App() {
    return (
        <Provider store={store}>
            <Dashboard/>
        </Provider>
    );
};

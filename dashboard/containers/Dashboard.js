import {Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {connect} from "react-redux";
import React from "react";
import {scanForDevices} from "../actions/actions";

const styles = StyleSheet.create({
    containerTitles: {
        flex: 0,
        marginVertical: 'auto',
        textAlignVertical: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
    },

    containerDevice: {
        flex: 0,
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold',
        // fontFamily: 'Oswald',
        marginVertical: 'auto',
        textAlignVertical: 'center',
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        alignContent: 'center',
        marginHorizontal: 'auto',
    },
    containerAppTitle: {
        flex: 0,
        fontSize: 18,
        color: 'white',
        fontWeight: '300',
        // fontFamily: 'Oswald',
        marginVertical: 'auto',
        marginHorizontal: 20,
    },
});
const Dashboard = ({startScanning, navigation, events}) => (
    <View style={{flex: 1, flexDirection: 'column'}}>
        <View style={{flex: 0.5, backgroundColor: 'orangered'}}/>

        <View
            style={{
                flex: 0.5,
                flexDirection: 'row',
                backgroundColor: 'orange',
            }}>
            <TouchableOpacity onPress={startScanning} style={styles.containerAppTitle}>
                <Image
                    source={{uri: 'https://facebook.github.io/react/logo-og.png'}}
                    style={{width: 40, height: 40}}
                />
            </TouchableOpacity>
            <View>
                <Text style={styles.containerAppTitle}> UV Sense </Text>
            </View>
        </View>
        <View style={{flex: 5}}>
            <View style={{flex: 1, flexDirection: 'column'}}>
                <View style={{flex: 1}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 1}}>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                <View style={{flex: 1}}>
                                    <Text style={styles.containerTitles}> Current UV </Text>
                                </View>
                                <View style={{flex: 3}}/>
                            </View>
                        </View>
                        <View style={{flex: 1}}>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                <View style={{flex: 1}}>
                                    <Text style={styles.containerTitles}> Protection </Text>
                                </View>
                                <View style={{flex: 3}}/>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={{flex: 1}}>
                    <View style={{flex: 1, flexDirection: 'row'}}>
                        <View style={{flex: 1}}>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                                <View style={{flex: 1}}>
                                    <Text style={styles.containerTitles}>
                                        {' '}
                                        Daily Doaasdgsagdsasage{' '}
                                    </Text>
                                </View>
                                <View style={{flex: 3}}/>
                            </View>
                        </View>
                        <View style={{flex: 1}}>
                            <View style={{flex: 1}}>
                                <View style={{flex: 1}}>
                                    <Text style={styles.containerTitles}> Time Left </Text>
                                </View>
                                <View style={{flex: 3}}/>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        </View>
        <View style={{flex: 1, backgroundColor: 'orange'}}>
            <View

                style={{
                    flex: 1,
                    marginHorizontal: 'auto',
                    alignContent: 'center',
                    flexDirection: 'row',
                    backgroundColor: 'orange',
                }}>
                <View>
                    <Text style={styles.containerDevice}/>
                </View>
                <TouchableOpacity onPress={() => navigation.navigate('Devices')}>
                    <Text style={styles.containerDevice}> Device Name </Text>
                </TouchableOpacity>
                <View>
                    <Text style={styles.containerDevice}> > </Text>
                </View>
            </View>
        </View>
    </View>
)


const mapStateToProps = state => ({
    // filters: getFilters(state),
    // getLookup: (lookup) => getLookup(state, lookup),
});

const mapDispatchToProps = dispatch => ({
    startScanning: () => dispatch(scanForDevices()),
    // getFilterHandler: (filterId, itemId) => e =>
    //   dispatch(filterEntity(filterId, itemId)),
});

export default connect(mapStateToProps, mapDispatchToProps,)(Dashboard)

// export default withSubscription({
//     subscriptionName: 'events',
//     destroyOnWillUnmount: true,
// })(MyComponent);

import React from "react";
import {FlatList, StyleSheet, Text} from "react-native"
import {connectToDevice, scanForDevices} from "../actions/actions"
import {connect} from "react-redux"
import {SafeAreaView} from "react-native-safe-area-context"
import {getDevices} from "../reducers/reducer"
import UvItem from "../components/UvItem"

function Devices({devices,connectDevice, navigation}) {
    const [selected, setSelected] = React.useState(new Map());

    const onSelect = React.useCallback(
        id => {
            connectDevice(id)
            const newSelected = new Map(selected);
            newSelected.set(id, !selected.get(id));

            setSelected(newSelected);
        },
        [selected],
    );

    return (
        <SafeAreaView style={styles.container}>
            <FlatList
                data={devices}
                renderItem={({item}) => (
                    <UvItem
                        id={item.id}
                        device={item}
                        selected={!!selected.get(item.id)}
                        onSelect={onSelect}
                    />
                )}
                keyExtractor={item => item.id}
                extraData={selected}
            />
            <Text>Unpaired</Text>
            <FlatList
                data={devices}
                renderItem={({item}) => (
                    <UvItem
                        id={item.id}
                        device={item}
                        selected={!!selected.get(item.id)}
                        onSelect={onSelect}
                    />
                )}
                keyExtractor={item => item.id}
                extraData={selected}
            />
        </SafeAreaView>
    );
}

// const Devices = ({devices, navigation}) => (
//     <SafeAreaView style={styles.container}>
//         <FlatList
//             data={devices}
//             renderItem={({item}) => (
//                 <UvItem.js
//                     id={item.id}
//                     name={item.name}
//                     selected={!!selected.get(item.id)}
//                     onSelect={onSelect}
//                 />
//             )}
//             keyExtractor={item => item.id}
//             extraData={selected}
//         />
//     </SafeAreaView>
// )
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    item: {
        backgroundColor: '#fab79b',
        padding: 10,
        marginVertical: 4,
        marginHorizontal: 16,
        borderRadius: 18
    },
    title: {
        fontSize: 32,
    },
});


const mapStateToProps = state => ({
    devices: getDevices(state)
// filters: getFilters(state),
// getLookup: (lookup) => getLookup(state, lookup),
});

const mapDispatchToProps = dispatch => ({
    startScanning: () => dispatch(scanForDevices()),
    connectDevice :(device) => dispatch(connectToDevice(device))
// getFilterHandler: (filterId, itemId) => e =>
//   dispatch(filterEntity(filterId, itemId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Devices)



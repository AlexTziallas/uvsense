import {SELECT_ENTITY, UPDATE_ENTITIES} from "../actions/actions";
import {combineReducers} from "redux";
import {REMOVE_ENTITY} from "../../transit/actions/actions";

export const getEntity = (state, entityId) => state.entities.byId[entityId]

const entity = (state = {}, action) => {
    switch (action.type) {
        case SELECT_ENTITY:
            return {
                ...state,
                selected: !state.selected
            }
        default:
            return state
    }
}

export const getEntities = (state) => state.entities.allIds

const entitiesById = (state = {}, action) => {
    switch (action.type) {
        case SELECT_ENTITY:
            return {
                ...state,
                [action.entityId]: entity(state[action.entityId], action)
            }
        case UPDATE_ENTITIES:
            return action.entities.byId
        case REMOVE_ENTITY:
            let {[action.entityId]: omit, ...rest} = state
            return rest
        default:
            return state
    }
}

const entitiesAllIds = (state = [], action) => {
    switch (action.type) {
        case UPDATE_ENTITIES:
            return action.entities.allIds
        case REMOVE_ENTITY:
            return state.filter(e => e !== action.entity)
        default:
            return state
    }
}


const entities = combineReducers({
    byId: entitiesById,
    allIds: entitiesAllIds
})

export default entities
import {
    BLE_STATE_UPDATED,
    CLEAR_DEVICES,
    ConnectionState,
    DEVICE_FOUND,
    UPDATE_CONNECTION_STATE
} from "../actions/actions";
import {combineReducers} from "redux";
import {State} from 'react-native-ble-plx';

export const getBleState = (state) => state.bleState

const bleState = (state = {status: State.Unknown, logs: []}, action) => {
    switch (action.type) {
        case BLE_STATE_UPDATED:
            return {
                ...state,
                status: action.status,
                logs: ['BLE state changed: ' + action.status, ...state.logs],
            };
        default:
            return state
    }
}

export const getKnownDevices = (state) => state.knownDevices.allIds.map(id => state.knownDevices.byId[id])

const knownDevicesById = (state = {}, action) => {
    switch (action.type) {
        case UPDATE_CONNECTION_STATE:
            if (action.connectionState === ConnectionState.CONNECTED)
                return {
                    ...state,
                    [action.device.id]: action.device
                }
            else if (action.connectedState === ConnectionState.DISCONNECTED) {
                const {[action.device.id]: removedDevice, ...newState} = state
                return newState
            } else {
                return state
            }
        default:
            return state
    }
}

const knownDevicesAllIds = (state = [], action) => {
    switch (action.type) {
        case UPDATE_CONNECTION_STATE:
            if (action.connectionState === ConnectionState.CONNECTED) {
                if (!state.includes(action.device.id)) return [action.device.id, ...state]
                else return state
            } else if (action.connectedState === ConnectionState.DISCONNECTED)
                return state.filter(d => d.id !== action.device.id)
            else
                return state
        default:
            return state
    }
}


const knownDevices = combineReducers({
    byId: knownDevicesById,
    allIds: knownDevicesAllIds
})


export const getDevices = (state) => state.devices.allIds.map(id => state.devices.byId[id])

export const getDevice = (state,deviceId) => state.devices.byId[deviceId]

const devicesById = (state = {}, action) => {
    switch (action.type) {
        case DEVICE_FOUND:
            return {
                ...state,
                [action.device.id]: action.device
            }
        case CLEAR_DEVICES:
            return {}
        default:
            return state
    }
}

const devicesAllIds = (state = [], action) => {
    switch (action.type) {
        case DEVICE_FOUND:
            if (!state.includes(action.device.id)) return [action.device.id, ...state]
            else return state
        case CLEAR_DEVICES:
            return []
        default:
            return state
    }
}


const devices = combineReducers({
    byId: devicesById,
    allIds: devicesAllIds
})

const reducer = combineReducers({
    knownDevices,
    devices,
    bleState
})

export default reducer;

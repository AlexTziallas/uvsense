import React from "react";
import {FlatList, Image, StyleSheet, Text, TouchableOpacity, View} from "react-native"
import {scanForDevices} from "../actions/actions"
import {connect} from "react-redux"
import {SafeAreaView} from "react-native-safe-area-context"
import {getDevices} from "../reducers/reducer"

export default function UvItem({id, device, selected, onSelect}) {
    return (
        <TouchableOpacity onPress={() => onSelect(id)}>
            <View style={[styles.item, {
                display: 'flex',
                alignItems: 'stretch',
                flexDirection: 'row',
                backgroundColor: selected ? '#6e3b6e' : '#f9c2ff'
            }]}>
                <Image
                    resizeMode='contain'
                    style={{flexBasis: 40, flexGrow: 1, width: undefined, height: undefined}}
                    source={require('../static/uvsense.png')}/>
                <View style={{
                    flexGrow: 2,
                    flexDirection: 'column',
                    paddingVertical:10,
                    paddingHorizontal: 6
                }}>

                    <Text style={{flexGrow: 1}}> {device.id} </Text>
                    <Text style={{flexGrow: 1}}> Unpaired </Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    item: {
        backgroundColor: '#fab79b',
        paddingHorizontal: 10,
        paddingVertical: 5,
        // marginVertical: 4,
        marginHorizontal: 16,
        borderRadius: 18
    },
    title: {
        fontSize: 32,
    },
});

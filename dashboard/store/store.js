import {applyMiddleware, compose, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
// import {composeWithDevTools} from "redux-devtools-extension";
import rootSaga from "../sagas/saga";
import reducer from "../reducers/reducer";
import { composeWithDevTools } from 'redux-devtools-extension';


const composeEnhancers = composeWithDevTools({
    // Specify name here, actionsBlacklist, actionsCreators and other options if needed
});

const sagaMiddleware = createSagaMiddleware();
export const store = createStore(
    reducer,
    {},
    composeEnhancers(applyMiddleware(sagaMiddleware)) ///to tyliges se ena compose Enhancers kai polla mazi
);
sagaMiddleware.run(rootSaga);

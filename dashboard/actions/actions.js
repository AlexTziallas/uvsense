export const SCAN_FOR_DEVICES = 'SCAN_FOR_DEVICES';
export const STOP_SCANNING = 'STOP_SCANNING';
export const BLE_STATE_UPDATED = 'BLE_STATE_UPDATED';
export const CONNECT_TO_DEVICE = 'CONNECT_TO_DEVICE'
export const DEVICE_FOUND = 'DEVICE_FOUND'
export const CLEAR_DEVICES = 'CLEAR_DEVICES'
export const UPDATE_CONNECTION_STATE = 'UPDATE_CONNECTION_STATE'
export const DEVICE_DATA = 'DEVICE_DATA'
export const LOG = 'LOG';
export const ConnectionState = {
    DISCONNECTED: 'DISCONNECTED',
    CONNECTING: 'CONNECTING',
    DISCOVERING: 'DISCOVERING',
    CONNECTED: 'CONNECTED',
    DISCONNECTING: 'DISCONNECTING',
};

export const deviceData = (deviceId,data) => ({
    type: DEVICE_DATA,
    deviceId,
    data
})

export const updateConnectionState = (connectionState,device) => ({
    type: UPDATE_CONNECTION_STATE,
    connectionState,
    device
})

export const clearDevices = () => ({
    type: CLEAR_DEVICES
})

export const deviceFound = (device) => ({
    type: DEVICE_FOUND,
    device
})

export const scanForDevices = () => ({
    type: SCAN_FOR_DEVICES,
});

export const stopScanning = () => ({
    type: STOP_SCANNING
})

export const bleStateUpdated = (status) => ({
    type: BLE_STATE_UPDATED,
    status
})

export const connectToDevice = (deviceId) => ({
    type: CONNECT_TO_DEVICE,
    deviceId
})

export const log = (message) => ({
    type: 'LOG',
    message
})
// export const setTransition = (transition) => ({
//     type: SET_TRANSITION,
//     transition
// })

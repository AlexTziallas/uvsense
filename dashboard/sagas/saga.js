import {buffers, eventChannel} from 'redux-saga';
import {call, cancelled, fork, put, select, take,} from 'redux-saga/effects';
import {CONNECT_TO_DEVICE, log} from '../actions/actions';
import {BleManager, LogLevel} from "react-native-ble-plx";
import {getBleState} from "../reducers/reducer";
import {handleBleState} from "./bleState"
import {handleScanning} from "./scanning"
import {handleConnection} from "./connection"

var base64 = require('base-64');
export default function* root() {
    yield put(log('BLE saga started...'));
    const manager = new BleManager();
    manager.setLogLevel(LogLevel.Verbose);
    // yield fork(bleWatcher, manager);
    yield fork(handleBleState, manager);
    yield fork(handleScanning, manager);
    yield fork(handleConnection, manager);
    // yield all([
    //     fork(bleWatcher),
    //     fork(handleBleState),
    // ]);
}

function* bleWatcher(manager) {
    try {
        while (true) {
            const bleState = yield select(getBleState)
            if (bleState.status === 'PoweredOn') {

                const scanningChannel = yield eventChannel(emit => {
                    console.log("SCANNING")
                    manager.startDeviceScan(
                        null,
                        {allowDuplicates: true},
                        (error, scannedDevice) => {
                            if (error) {
                                emit([error, scannedDevice])
                                return
                            }
                            if (scannedDevice != null && scannedDevice.localName === 'HM19') {
                                emit([error, scannedDevice])
                            }
                        },
                    )
                    return () => {
                        manager.stopDeviceScan()
                    }
                }, buffers.expanding(1))


                try {
                    for (; ;) {
                        const [error, device] = yield take(scanningChannel);
                        if (error != null) {
                            console.log(error)
                        }
                        if (device != null) {
                            if (device.name === "HM19") {
                                // Stop scanning as it's not necessary if you are scanning for one device.
                                manager.stopDeviceScan();
                                const conDevice = yield device.connect()
                                yield device.discoverAllServicesAndCharacteristics()
                                const services = yield conDevice.services()
                                console.log(services)
                                for (let service of services) {
                                    const characteristics = yield service.characteristics()
                                    const car = yield characteristics[0].read()

                                    const readC = yield call([car, car.read])
                                    console.log(readC)

                                    const descriptors = yield call([
                                        car,
                                        car.descriptors,
                                    ]);


                                    for (const descriptor of descriptors) {
                                        const d = yield call([descriptor, descriptor.read]);
                                        console.log(d)
                                    }
                                    device.monitorCharacteristicForService(
                                        service.uuid,
                                        car.uuid,
                                        (error, characteristic) => {
                                            console.log(base64.decode(characteristic.value))
                                        }
                                    )

                                }
                            }
                        }
                    }
                } catch
                    (error) {
                    console.log(error)
                } finally {
                    if (yield cancelled()) {
                        scanningChannel.close()
                    }
                }


// hm19.connect()
//     .then((device) => {
//         return device.discoverAllServicesAndCharacteristics()
//     })
//     .then((device) => {
//         this.manager.servicesForDevice(device.id)
//             .then((servicesForDevice) => {
//                 console.log("servicesForDevice");
//                 console.log(servicesForDevice);
//                 return (servicesForDevice);
//             })
//             .then((servicesForDevice) => {
//                 for (let s of servicesForDevice) {
//                     this.manager.characteristicsForDevice(s.deviceID, s.uuid)
//                         .then((characteristics) => {
//                             console.log("characteristics");
//                             console.log(characteristics[0]);
//
//                             if (characteristics[0].isWritableWithoutResponse) {
//                                 characteristics[0].writeWithoutResponse("1B 40");
//                                 characteristics[0].writeWithoutResponse("0A");
//                                 characteristics[0].writeWithoutResponse("0A");
//                             }
//                         })
//                 }
//             }).catch(err => console.log(err))
//
//         // console.log(manager.servicesForDevice(device.uuid))
//         // manager.readCharacteristicForDevice(device)
//         // Do work on device with services and characteristics
//     })
//     .catch((error) => {
//         console.log(err)
//         // Handle errors
//     });
            }
        }
    } finally {
        //     if (yield cancelled()) {
        //         stateChannel.close();
        //     }
    }

}


function* handluConnection(manager) {
    var testTask = null;

    for (; ;) {
        // Take action
        const {device} = yield take(CONNECT_TO_DEVICE);

        const disconnectedChannel = yield eventChannel(emit => {
            const subscription = device.onDisconnected(error => {
                emit({type: 'DISCONNECTED', error: error});
            });
            return () => {
                subscription.remove();
            };
        }, buffers.expanding(1));

        const deviceActionChannel = yield actionChannel([
            'DISCONNECT',
            'EXECUTE_TEST',
        ]);

        try {
            yield put(updateConnectionState(ConnectionState.CONNECTING));
            yield call([device, device.connect]);
            yield put(updateConnectionState(ConnectionState.DISCOVERING));
            yield call([device, device.discoverAllServicesAndCharacteristics]);
            yield put(updateConnectionState(ConnectionState.CONNECTED));

            for (; ;) {
                const {deviceAction, disconnected} = yield race({
                    deviceAction: take(deviceActionChannel),
                    disconnected: take(disconnectedChannel),
                });

                if (deviceAction) {
                    if (deviceAction.type === 'DISCONNECT') {
                        yield put(log('Disconnected by user...'));
                        yield put(updateConnectionState(ConnectionState.DISCONNECTING));
                        yield call([device, device.cancelConnection]);
                        break;
                    }
                    if (deviceAction.type === 'EXECUTE_TEST') {
                        if (testTask != null) {
                            yield cancel(testTask);
                        }
                        testTask = yield fork(executeTest, device, deviceAction);
                    }
                } else if (disconnected) {
                    yield put(log('Disconnected by device...'));
                    if (disconnected.error != null) {
                        yield put(logError(disconnected.error));
                    }
                    break;
                }
            }
        } catch (error) {
            yield put(logError(error));
        } finally {
            disconnectedChannel.close();
            yield put(testFinished());
            yield put(updateConnectionState(ConnectionState.DISCONNECTED));
        }
    }
}










import {
    BLE_STATE_UPDATED,
    CONNECT_TO_DEVICE,
    deviceFound,
    log,
    SCAN_FOR_DEVICES,
    STOP_SCANNING, stopScanning
} from "../actions/actions"
import {buffers, eventChannel} from "redux-saga"
import {actionChannel, take, cancel, cancelled, fork, put, call, delay} from "redux-saga/effects"
import {State} from "react-native-ble-plx/src/TypeDefinition"
import * as PermissionsAndroid from "react-native"

export function* handleScanning(manager) {
    let scannning
    let scanTask = null
    let delayedCancelTask = null
    let bleState = State.Unknown
    // var connectionState = ConnectionState.DISCONNECTED

    const channel = yield actionChannel([
        STOP_SCANNING,
        SCAN_FOR_DEVICES,
        CONNECT_TO_DEVICE,
        BLE_STATE_UPDATED
    ]);

    while (true) {
        const action = yield take(channel);

        switch (action.type) {
            case STOP_SCANNING:
            case CONNECT_TO_DEVICE:
                scannning = false
                break;
            case SCAN_FOR_DEVICES:
                scannning = true
                break;
            case BLE_STATE_UPDATED:
                bleState = action.status
                break;
        }

        if (scannning) {
            if (bleState === State.PoweredOn) {
                if (scanTask != null) yield cancel(scanTask);
                delayedCancelTask = yield fork(delayedStopScan)
                scanTask = yield fork(scan, manager);
            } else {
                //TOAST
            }
        } else {
            if (delayedCancelTask != null) {
                yield cancel(delayedCancelTask);
                delayedCancelTask = null;
            }
            if (scanTask != null) {
                yield cancel(scanTask);
                scanTask = null;
            }
        }
    }
}


function* delayedStopScan() {
    yield delay(6000)
    yield put(stopScanning())
}

// As long as this generator is working we have enabled scanning functionality.
// When we detect SensorTag device we make it as an active device.
function* scan(manager) {
    const androidPermission = yield call(checkAndroidPermission)
    if (androidPermission) {
        yield put(log('Scanning started...'));
        const scanningChannel = yield eventChannel(emit => {
            manager.startDeviceScan(null, {allowDuplicates: true},
                (error, scannedDevice) => {
                    if (error) {
                        emit([error, scannedDevice]);
                        return;
                    }
                    // if (scannedDevice != null && scannedDevice.localName === 'HM19') {
                    if (scannedDevice != null) {
                        emit([error, scannedDevice]);
                    }
                },
            );
            return () => {
                manager.stopDeviceScan();
            };
        }, buffers.expanding(1));

        try {
            while (true) {
                const [error, scannedDevice] = yield take(scanningChannel)
                if (error != null) {
                }
                if (scannedDevice != null && scannedDevice.name === 'HM19') {
                    yield put(deviceFound(scannedDevice))
                }
            }
        } catch (err) {
            console.log(err)
            yield put(log(err))
            // console.log(error)
        } finally {
            yield put(log('Scanning stopped...'));
            if (yield cancelled()) {
                // scanningChannel.close();
            }
        }
    }
}

function* checkAndroidPermission() {
    if (Platform.OS === 'android' && Platform.Version >= 23) {
        yield put(log('Scanning: Checking permissions...'));
        const enabled = yield call(
            PermissionsAndroid.check,
            PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        );
        if (!enabled) {
            yield put(log('Scanning: Permissions disabled, showing...'));
            const granted = yield call(
                PermissionsAndroid.request,
                PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
            );
            if (granted !== PermissionsAndroid.RESULTS.GRANTED) {
                yield put(log('Scanning: Permissions not granted, aborting...'));
                // TODO: Show error message?
                return false
            }
        }
    } else return true
}

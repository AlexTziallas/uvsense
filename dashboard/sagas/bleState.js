import {buffers, eventChannel} from "redux-saga"
import {bleStateUpdated} from "../actions/actions"
import {cancelled, put, take} from "redux-saga/effects"

export function* handleBleState(manager) {
    const stateChannel = yield eventChannel(emit => {
        const subscription = manager.onStateChange(state => {
            emit(state);
        }, true);
        return () => {
            subscription.remove();
        };
    }, buffers.expanding(1));

    try {
        for (; ;) {
            const newState = yield take(stateChannel);
            yield put(bleStateUpdated(newState));
        }
    } finally {
        if (yield cancelled()) {
            stateChannel.close();
        }
    }
}

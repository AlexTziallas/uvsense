import {CONNECT_TO_DEVICE, ConnectionState, updateConnectionState, log} from "../actions/actions"
import {actionChannel, put, take, call, select, fork} from "redux-saga/effects"
import {buffers, eventChannel} from "redux-saga"
import {getDevice, getDevices} from "../reducers/reducer"

var base64 = require('base-64');
export function* handleConnection(manager) {
    var testTask = null

    while (true) {
        const action = yield take(CONNECT_TO_DEVICE)
        const device = yield select(getDevice, action.deviceId)
        const disconnectedChannel = yield eventChannel(emit => {
            const subscription = device.onDisconnected(error => {
                emit({type: 'DISCONNECTED', error: error});
            });
            return () => {
                subscription.remove();
            };
        }, buffers.expanding(1));

        const deviceActionChannel = yield actionChannel([
            'DISCONNECT',
            'EXECUTE_TEST',
        ]);

        yield fork(connectToDevice, device)

    }
}


function* connectToDevice(device) {
    try {
        yield put(updateConnectionState(ConnectionState.CONNECTING, device));
        yield call([device, device.connect]);
        yield call([device, device.discoverAllServicesAndCharacteristics]);
        yield put(updateConnectionState(ConnectionState.CONNECTING, device));
        yield call([device, device.connect]);
        yield put(updateConnectionState(ConnectionState.DISCOVERING, device));
        yield call([device, device.discoverAllServicesAndCharacteristics]);
        const services = yield call([device, device.services]);
        yield put(updateConnectionState(ConnectionState.CONNECTED, device));

        for (let service of services) {
            if (service.isPrimary) {
                const characteristics = yield service.characteristics()
                const char = yield characteristics[0].read()
                device.monitorCharacteristicForService(
                    service.uuid,
                    char.uuid,
                    (error, characteristic) => {
                        console.log(base64.decode(characteristic.value))
                    }
                )
            }
            console.log(service)
            yield put(log('Found service: ' + service.uuid));
        }

    } catch (error) {
        console.log(error)
        yield put(log(error))
        yield put(updateConnectionState(ConnectionState.DISCONNECTED));
    } finally {
        yield put(updateConnectionState(ConnectionState.DISCONNECTED));
    }
}

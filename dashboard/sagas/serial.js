// import {
//   API_RESPONSE,
//   apiResponse,
//   BARCODE_CHANGED,
//   clearBarcode,
//   REMOVE_BARCODE,
//   SEARCH_BARCODE,
//   searchBarcode,
//   updateEntities,
// } from '../actions/actions';
import {eventChannel} from 'redux-saga'
import {all, debounce, fork, cancelled, take} from 'redux-saga/effects';
import BluetoothSerial from 'react-native-bluetooth-serial'
// import 'regenerator-runtime/runtime';
// import {Api} from '../services/api';
// import {hideLoader, showLoader} from '../../search/actions/actions';
// import {timeAndDisplayMessages} from '../../common/sagas/messages';
// import {
//   DESIGNATE,
//   SELECT_ENTITY,
//   selectEntity,
//   TRANSFER,
// } from '../../common/actions/actions';
// import {designate, transfer} from '../../common/sagas/locations';
// import {getSelections} from '../../common/reducers/selections';
import {BleManager} from 'react-native-ble-plx';
import {SCAN_FOR_DEVICES} from '../actions/actions';

// function* selectLine() {
//   const selections = yield select(getSelections);
//   for (var selection of selections) {
//   }
// }
//
// function* selectLineWatcher() {
//   yield takeEvery(SELECT_ENTITY, selectLine);
// }
//
// function* removeBarcode(action) {
//   const json = yield call(Api.removeBarcode, action.barcode);
//   yield put(updateEntities(json));
//   yield put(apiResponse('removeBarcode', json));
//
//   //selecting an already existing entity will remove it from selections ;)
//   yield put(selectEntity(action.barcode));
// }
//
// function* removeBarcodeWatcher() {
//   yield takeEvery(REMOVE_BARCODE, removeBarcode);
// }
//
// function* designateWatcher() {
//   yield takeEvery(DESIGNATE, designate);
// }
//
// function* transferWatcher() {
//   yield takeEvery(TRANSFER, transfer);
// }
//
// function* barcodeChange(action) {
//   if (action.barcode.length > 4) {
//     yield put(searchBarcode(action.barcode));
//     const json = yield call(Api.searchBarcode, action.barcode);
//     yield put(apiResponse('barcodeChanged', json));
//     switch (json.result) {
//       case 'found':
//         yield put(updateEntities(json));
//         yield put(clearBarcode());
//         yield put(selectEntity(action.barcode));
//         break;
//       case 'invalid':
//       case 'exists':
//         yield call(timeAndDisplayMessages, json.messages);
//         break;
//     }
//   }
// }
//
// function* barcodeChangeWatcher() {
//   yield debounce(520, BARCODE_CHANGED, barcodeChange);
// }
//
// function* loaderWatcher() {
//   let pendingCalls = 0;
//   const channel = yield actionChannel([
//     SEARCH_BARCODE,
//     API_RESPONSE,
//     REMOVE_BARCODE,
//     DESIGNATE,
//     TRANSFER,
//   ]);
//   while (true) {
//     const action = yield take(channel);
//     switch (action.type) {
//       case SEARCH_BARCODE:
//       case REMOVE_BARCODE:
//       case TRANSFER:
//       case DESIGNATE:
//         yield put(showLoader());
//         pendingCalls++;
//         break;
//       case API_RESPONSE:
//         pendingCalls--;
//         if (pendingCalls === 0) {
//           yield put(hideLoader());
//         }
//         break;
//     }
//   }
// }

function* scanDevices() {
}

function* bleWatcher() {
    // First step is to create BleManager which should be used as an entry point
    // to all BLE related functionalities
    Promise.all([
        BluetoothSerial.isEnabled(),
        BluetoothSerial.list()
    ]).then((values) => {
        const [isEnabled, devices] = values
        if (isEnabled) {
            for (const device in devices) {
                console.log(device)
            }
        }
        this.setState({isEnabled, devices})

    })

    // BluetoothSerial.on('bluetoothEnabled', () => {
    //
    //     Promise.all([
    //         BluetoothSerial.isEnabled(),
    //         BluetoothSerial.list()
    //     ])
    //         .then((values) => {
    //             const [isEnabled, devices] = values
    //
    //         })
    //
    //     BluetoothSerial.on('bluetoothDisabled', () => {
    //
    //         this.setState({devices: []})
    //
    //     })
    //     BluetoothSerial.on('error', (err) => console.log(`Error: ${err.message}`))
    //
    // })


    // const subscription = manager.onStateChange((state) => {
    //     if (state === 'PoweredOn') {
    //
    //         yield debounce(1000, SCAN_FOR_DEVICES, scanDevices);
    //         subscription.remove();
    //     }
    // }, true);

    // manager.setLogLevel(LogLevel.Verbose);

    // All below generators are described below...
    // yield fork(handleScanning, manager);
    // yield fork(handleBleState, manager);
    // yield fork(handleConnection, manager);
}

export default function* root() {
    yield all([
        fork(bleWatcher),
        // fork(selectLineWatcher),
        // fork(removeBarcodeWatcher),
        // fork(transferWatcher),
        // fork(loaderWatcher),
        // fork(designateWatcher),
    ]);
}
